<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<c:if test="${not empty param.error}">
    <div class="alert alert-danger" role="alert">ошибка: ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</div>
</c:if>
<c:if test="${not empty param.successreg}">
    <div class="alert alert-success" role="alert">Операция успешно завершена</div>
</c:if>

<form method="POST" action="<c:url value="/login" />" class="form-signin">
    <div class="box">
        <!-- Box Head -->
            <h2 class="form-signin-heading">Вход</h2>

        <label for="inputEmail" class="sr-only">логин</label>
        <input type="text" id="inputEmail" name="username"  class="form-control" placeholder="Имя пользователя" required autofocus>
        <label for="inputPassword" class="sr-only">пароль</label>
        <input type="password" name="password"  id="inputPassword" class="form-control" placeholder="Пароль" required>
        <div class="checkbox">
          <label>
            <input type="checkbox" name="_spring_security_remember_me" /> запомнить
          </label>
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Войти</button>

    </div>
<%--<div align="center">
    <a href="<spring:url value="/addnewacc" />" class="fill_href">Регистрация</a>
</div>--%>

</form>
