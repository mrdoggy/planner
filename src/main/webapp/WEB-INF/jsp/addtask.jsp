<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Настройки задачи</title>
    <link type="text/css" href="<spring:url value="/css/bootstrap-datetimepicker.min.css" />" rel="stylesheet" />
    <script type="text/javascript" src="<spring:url value="/js/moment.min.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/transition.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/collapse.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/bootstrap-datetimepicker.min.js" />"></script>
    <script type="text/javascript">
        $(function(){
            $('#dateEnd').datetimepicker({
                locale: 'ru',
                format: 'YYYY-MM-DD',
                showTodayButton: true,
                allowInputToggle: true
            });
            <c:if test="${!empty(votesList)}">
            var votes = [${votesList}];
            $.each(votes, function(i, value) {
                $('#votes')
                        .append($("<option></option>")
                                .text(value));
            });
            </c:if>
        });
     </script>

</head>
<body>
<spring:url value="/addtask" var="postUrl" />
<form:form method="post" action="${postUrl}" commandName="taskDTO">
    <form:hidden path="id" />
    <c:if test="${ok}">
        <!-- Message OK -->
        <div class="alert alert-success" role="alert">Данные сохранены успешно</div>
        <!-- End Message OK -->
    </c:if>

    <c:if test="${error}">
    <!-- Message Error -->
        <div class="alert alert-danger" role="alert">Ошибка сохранения</div>
    <!-- End Message Error -->
    </c:if>

    <div class="container-fluid">
    <!-- Box Head -->
    <h2 class="page-header">Информация о задаче</h2>

    <!-- Form -->
        <div class="form-group">
            <div class="errors"> <form:errors path="title" /></div>
            <label for="title">Название задачи: </label>
            <form:input path="title" cssClass="form-control" />
        </div>
        <div class="form-group">
            <div class="errors"> <form:errors path="description" /></div>
            <label for="description">Описание: </label>
            <form:textarea rows="7" path="description" cssClass="form-control" />
        </div>

        <div class="form-group">
            <div class="errors"> <form:errors path="priority" /></div>
            <label for="title">Приоритет: </label>
            <form:select path="priority" items="${priorityList}" cssClass="form-control" />
        </div>
        <div class="form-group">
            <div class="errors"> <form:errors path="dateEnd" /></div>
            <label for="dateEnd">Срок выполнения: </label>

            <div class='input-group date' id='dateEnd'>
                <form:input path="dateEnd" cssClass="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
            </div>
        </div>

        <div class="form-group">
            <div class="errors"> <form:errors path="votes" /></div>
            <label for="votes">Голосовалка: <a href="javascript:;" id="add_vote">+</a> / <a href="javascript:;" id="rem_vote">-</a></label>
            <select name="votes" id="votes" multiple class="form-control" >
            </select>
        </div>
    <!-- End Form -->

    <button class="btn btn-primary" type="submit" id="submit">Сохранить</button>
    <a href="<spring:url value="/" />" class="btn btn-default">Отмена</a>

    </div>

</form:form>   <br /><br /><br />
<script type="text/javascript">

    $('#submit').click(function(){
        $('#votes option').prop('selected', true);
        return true;
    });
    $('#add_vote').click(function(){
        var vote = window.prompt('Введите вариант:');
        if (vote != null) {
            if (optionExists(vote)) {
                alert('Такой вопрос уже существует');
            } else {
                $('#votes').append('<option>' + vote + '</option>');
            }
        }
    });
    $('#rem_vote').click(function(){
        var to_rem = [];
        $('#votes :selected').each(function(i, selected){
            to_rem.push(selected.index);
        });
        var to_show = [];
        $('#votes option').each(function(i, el){
            if ($.inArray(i, to_rem) == -1)
                to_show.push($(el).text());
        });
        $('#votes').empty();
        $.each(to_show, function(i, value) {
            $('#votes')
                    .append($("<option></option>")
                            .text(value));
        });
    });

    function optionExists(val) {
        var exists = false;
        $('#votes option').each(function(){
            if (this.value == val) {
                exists = true;
                return false;
            }
        });
        return exists;
    }

</script>
</body>
