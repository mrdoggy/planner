<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Настройки пользователя</title>
</head>
<body>
<spring:url value="/adduser" var="postUrl" />
<form:form method="post" action="${postUrl}" commandName="userDTO">
    <form:hidden path="id" />
    <c:if test="${ok}">
        <!-- Message OK -->
        <div class="alert alert-success" role="alert">Данные сохранены успешно</div>
        <!-- End Message OK -->
    </c:if>

    <c:if test="${error == 1}">
    <!-- Message Error -->
        <div class="alert alert-danger" role="alert">Ошибка: неверный пароль</div>
    <!-- End Message Error -->
    </c:if>
    <c:if test="${error == 2}">
        <!-- Message Error -->
        <div class="alert alert-danger" role="alert">Ошибка: пользователь с таким именем существует</div>
        <!-- End Message Error -->
    </c:if>

    <div class="container-fluid">
    <!-- Box Head -->
    <h2 class="page-header">Персональные данные</h2>

    <!-- Form -->
        <div class="form-group">
            <div class="errors"> <form:errors path="username" /></div>
            <label for="username">Имя пользвателя: </label>
            <form:input path="username" cssClass="form-control" />
        </div>
        <div class="form-group">
            <div class="errors"> <form:errors path="email" /></div>
            <label for="email">Email: </label>
            <form:input path="email" cssClass="form-control" />
        </div>
        <div class="form-group">
            <label>
            <form:checkbox path="enabled" value="1" /> Активный</label>
        </div>
        <div class="form-group">
            <label>
            <input type="checkbox" name="is_admin" id="is_admin" value="1"<c:if test="${not empty is_admin}"> checked="checked" </c:if> /> Администратор
             </label>
        </div>
    <div class="form-group">
            <label for="password">Пароль: </label>
            <input type="password" id="password" name="password" value="" class="form-control" />
    </div>
        <div class="form-group">
            <label for="password_confirm">Подтвердите пароль: </label>
            <input type="password" id="password_confirm" name="password_confirm" value="" class="form-control" />
        </div>

    <!-- End Form -->

    <button class="btn btn-primary" type="submit">Сохранить</button>
    <a href="<spring:url value="/users" />" class="btn btn-default">Отмена</a>

    </div>

</form:form>   <br /><br /><br />
</body>
