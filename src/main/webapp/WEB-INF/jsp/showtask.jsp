<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>${taskDTO.title}</title>
</head>
<body>
<c:if test="${param.vote == 'ok'}">
    <div class="alert alert-success" role="alert">Спасибо, ваш голос учтен</div>
</c:if>
<c:if test="${param.comment == 'ok'}">
    <div class="alert alert-success" role="alert">Спасибо, ваш комментарий сохранен</div>
</c:if>
<h1 class="page-header">${taskDTO.title}</h1>
<div align="right">
    <c:choose>
    <c:when test="${bookmarked}">
        <a href="<spring:url value="/task/bookmark/drop/" />${taskDTO.id}" class="btn btn-danger" > <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Удалить из закладок</a>
    </c:when>
    <c:otherwise>
        <a href="<spring:url value="/task/bookmark/add/" />${taskDTO.id}" class="btn btn-success" > <span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Добавить в закладки</a>
    </c:otherwise>
    </c:choose>
</div>

${taskDTO.description}
<br /><br /><br />
<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <h4>Приоритет</h4>
        <span class="text-muted">${taskDTO.priority}</span>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h4>Срок выполнения</h4>
        <span class="text-muted"><fmt:formatDate pattern="yyyy-MM-dd" value="${taskDTO.dateEnd}" /></span>
    </div>
</div>

<spring:url value="/task/addvote" var="postUrl" />
<form method="post" action="${postUrl}">
    <input type="hidden" name="task_id" value="${taskDTO.id}" />
<div class="row placeholders">
    <div class="col-xs-6 col-sm-3 placeholder">
        <h4>Голосование</h4>
    <c:forEach items="${taskDTO.getTaskVotes()}" var="taskVote">
        <label>
        <input type="radio" name="vote" value="${taskVote.id}" /> ${taskVote.title}</label> <br />
    </c:forEach> <br />
        <button class="btn btn-primary" type="submit" > Голосовать</button>
    </div>
    <div class="col-xs-6 col-sm-3 placeholder">
        <h4>Результаты</h4>
        <c:forEach items="${taskDTO.getTaskVotes()}" var="taskVote">
            ${taskVote.title}: <img src="<spring:url value="/img/progress.jpg" />" height="15" width="${taskVote.votes * 10}" /> ${taskVote.votes} <br />
        </c:forEach>
    </div>
</div>
</form>

<h3 class="page-header">Комментарии</h3>
   <c:choose>
    <c:when test="${!empty comments}">
        <c:forEach items="${comments}" var="comment">
         <span style="font-style: italic"><fmt:formatDate pattern="yyyy-MM-dd" value="${comment.added}" /></span> <span class="badge">${comment.user.username}</span><br />
         ${comment.comment}
         <br /><br />
        </c:forEach>
    </c:when>
    <c:otherwise>
    <div align="center">Комментариев не найдено</div>
    </c:otherwise>
    </c:choose>

<spring:url value="/task/comment" var="postUrl" />
<form method="post" action="${postUrl}">
    <input type="hidden" name="task_id" value="${taskDTO.id}" />
    <label for="comments">Комментарий</label>
    <textarea name="comments" id="comments" rows="7" placeholder="Добавьте комментарий..."  class="form-control"></textarea>
    <br />
    <button class="btn btn-primary" type="submit" > Отправить</button>
</form>

</body>
