<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><decorator:title default="Регистрация пользователей"/></title>
    <script type="text/javascript" src="<spring:url value="/js/jquery.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/jquery-ui.js" />"></script>
    <link type="text/css" href="<spring:url value="/css/jquery-ui.css" />" rel="stylesheet" />
    <decorator:head/>

    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<spring:url value="/css/bootstrap.min.css" />" rel="stylesheet" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link type="text/css" href="<spring:url value="/css/ie10-viewport-bug-workaround.css" />" rel="stylesheet" />
    <link type="text/css" href="<spring:url value="/css/main.css" />" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script type="text/javascript" src="<spring:url value="/js/ie8-responsive-file-warning.js" />"></script><![endif]-->
    <script type="text/javascript" src="<spring:url value="/js/ie-emulation-modes-warning.js" />"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body id="main">

    <div class="container">

        <decorator:body/>


    </div> <!-- /container -->


    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="<spring:url value="/js/ie10-viewport-bug-workaround.js" />"></script>
  </body>
</html>
