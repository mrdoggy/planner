<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://www.opensymphony.com/sitemesh/decorator" prefix="decorator" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <title><decorator:title default="Планировщик"/></title>
    <script type="text/javascript" src="<spring:url value="/js/jquery.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/jquery-ui.js" />"></script>
    <link type="text/css" href="<spring:url value="/css/jquery-ui.css" />" rel="stylesheet" />
    <link type="text/css" href="<spring:url value="/css/bootstrap-switch.css" />" rel="stylesheet" />
    <decorator:head/>

    <!-- Bootstrap core CSS -->
    <link type="text/css" href="<spring:url value="/css/bootstrap.min.css" />" rel="stylesheet" />

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link type="text/css" href="<spring:url value="/css/ie10-viewport-bug-workaround.css" />" rel="stylesheet" />
    <link type="text/css" href="<spring:url value="/css/main.css" />" rel="stylesheet" />

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script type="text/javascript" src="<spring:url value="/js/ie8-responsive-file-warning.js" />"></script><![endif]-->
    <script type="text/javascript" src="<spring:url value="/js/ie-emulation-modes-warning.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/bootstrap-switch.js" />"></script>

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

 <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<spring:url value="/" />">Планировщик</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="<spring:url value="/settings" />">${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username}</a></li>
            <li><a href="<spring:url value="/logout" />">Выход</a></li>
          </ul>
            <form class="navbar-form navbar-right" action="<spring:url value="/" />" method="get">
                <input type="text" name="q" class="form-control" placeholder="Поиск...">
            </form>
        </div>
      </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
            <li <c:if test="${action eq 'tasks'}">class="active"</c:if>><a href="<spring:url value="/" />"><span class="glyphicon glyphicon-list" aria-hidden="true"></span> Задачи <span class="sr-only">(current)</span></a></li>
            <li <c:if test="${action eq 'bookmarks'}">class="active"</c:if>><a href="<spring:url value="/bookmarks" />"><span class="glyphicon glyphicon-bookmark" aria-hidden="true"></span> Закладки <span class="sr-only">(current)</span></a></li>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li <c:if test="${action eq 'users'}">class="active"</c:if>><a href="<spring:url value="/users" />"><span class="glyphicon glyphicon-user" aria-hidden="true"></span> Пользователи</a></li>
            </sec:authorize>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

          <decorator:body/>

        </div>
      </div>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <script type="text/javascript" src="<spring:url value="/js/holder.min.js" />"></script>
    <script type="text/javascript" src="<spring:url value="/js/ie10-viewport-bug-workaround.js" />"></script>
  </body>
</html>
