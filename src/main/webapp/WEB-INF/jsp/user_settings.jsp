<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<head>
    <title>Настройки пользователя</title>
</head>
<body>


    <c:if test="${ok}">
    <!-- Message OK -->
        <div class="alert alert-success" role="alert">Данные сохранены успешно</div>
    <!-- End Message OK -->
    </c:if>

    <c:if test="${error}">
    <!-- Message Error -->
        <div class="alert alert-danger" role="alert">Ошибка: неверный пароль</div>
    <!-- End Message Error -->
    </c:if>

<div class="container-fluid">
<!-- Box Head -->
    <h2 class="page-header">Персональные данные</h2>
    <spring:url value="/settings" var="postUrl" />
    <form:form method="post" action="${postUrl}" commandName="user">

    <!-- Form -->
    <div class="form-group">
        <label for="email">Имя пользователя: </label>
        <input type="text" id="username" name="username" value="${user.username}" class="form-control" readonly />
    </div>

    <div class="form-group">
        <label for="email">Email: </label>
        <input type="text" id="email" name="email" value="${user.email}" class="form-control" />
</div>
    <div class="form-group">
        <label for="password">Пароль: </label>
        <input type="password" id="password" name="password" value="" class="form-control" />
</div>
    <div class="form-group">
        <label for="password2">Подтвердите пароль: </label>
        <input type="password" id="password2" name="password_confirm" value="" class="form-control" />
</div>
<!-- End Form -->

    <button class="btn btn-primary" type="submit">Сохранить</button>

</div>

</form:form>
</body>
