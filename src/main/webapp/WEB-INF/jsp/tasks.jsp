<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<head>
    <title>Список задач</title>
    <script type="text/javascript">
        $(function(){
            $('.remove').click(function(){
               return confirm('Вы действительно хотите удалить эту задачу?');
            });
        });
    </script>

</head>
<body>

          <h1 class="page-header">Задачи</h1>
          <c:if test="${!empty q}">Вы искали: <b>${q}</b><br /><br /></c:if>
<sec:authorize access="hasRole('ROLE_ADMIN')">
          <a href="<spring:url value="/addtask" />" class="btn btn-success">
              <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Создать задачу
          </a>
</sec:authorize>
<c:choose>
<c:when test="${!empty tasksList}">
<div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Название</th>
                  <th>Приоритет</th>
                  <th>Срок выполнения</th>
                  <th>Действия</th>
                </tr>
              </thead>
              <tbody>
              <c:forEach items="${tasksList}" var="task">
                <tr>
                  <td>${task.id}</td>
                  <td>${task.title}</td>
                  <td>${task.priority}</td>
                  <td><fmt:formatDate pattern="yyyy-MM-dd" value="${task.dateEnd}" /></td>
                  <td>
                      <a href="<spring:url value="/task/" />${task.id}" class="btn btn-primary">  <span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></a>
                      <sec:authorize access="hasRole('ROLE_ADMIN')">
                      <a href="<spring:url value="/task/edit/" />${task.id}" class="btn btn-info">  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                      <a href="<spring:url value="/task/delete/" />${task.id}" class="btn btn-danger remove"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a>
                      </sec:authorize>
                  </td>
                </tr>
              </c:forEach>
              </tbody>
            </table>


</c:when>
<c:otherwise>
    <div style="margin:20px;"><b>Ничего не найдено</b></div>
</c:otherwise>
</c:choose>

          </div>
</body>
