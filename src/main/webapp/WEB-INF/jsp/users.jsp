<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<head>
    <script type="text/javascript">
        var currentUser = '${sessionScope.SPRING_SECURITY_CONTEXT.authentication.principal.username}';
        $(function(){
            $("[name='active']").bootstrapSwitch({
               size: "mini",
               onSwitchChange: changeActiveStatus
            });

            $('.del').on('click',function(event){
               if ($(event.target).attr('ref') == currentUser) {
                           alert('Вы не можете удалить сами себя');
                           return false;
                        }
                return confirm('Вы действительно хотите удалить пользователя?');
            });
         });

         function changeActiveStatus(event, state) {
            if ($(event.target).attr('ref') == currentUser) {
               alert('Вы не можете деактивировать сами себя');
               return false;
            }
            if (state) {
                $('#changeForm').attr('action','${postUrl}users/user-enable/' + $(event.target).attr('ref'));
                $('#changeForm').submit();
            }
            else {
                $('#changeForm').attr('action','${postUrl}users/user-disable/' + $(event.target).attr('ref'));
                $('#changeForm').submit();
            }
         }
    </script>
</head>
<body>

<h1 class="page-header">Пользователи</h1>
<a href="<spring:url value="/adduser" />" class="btn btn-success">
    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Добавить пользователя
</a>
            <c:choose>
            <c:when test="${!empty usersList}">
            <!-- Table -->
                <div class="table-responsive">
                <table  class="table table-striped">
                    <thead>
                    <tr>
                        <th>Имя пользователя</th>
                        <th>Email</th>
                        <th>Дата добавления</th>
                        <th>Активен?</th>
                        <th>Роль</th>
                        <th width="100" class="ac">контроль</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${usersList}" var="user">
                        <tr>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>>${user.username}</td>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>>${user.email}</td>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>><fmt:formatDate dateStyle="full" pattern="dd.MM.yyyy HH:mm" value="${user.added}"/></td>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>>
                             <input type="checkbox" name="active" value="1" ref="${user.username}" <c:if test="${user.enabled}"> checked="checked"</c:if>/>
                             </td>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>><c:forEach items="${user.roles}" var="role">${role}<br /></c:forEach></td>
                            <td<c:if test="${not user.enabled}"> class="reged"</c:if>>
                             <a href="<spring:url value="/user/edit/" />${user.username}" class="btn btn-info">  <span class="glyphicon glyphicon-edit" aria-hidden="true"></span></a>
                             <a href="<spring:url value="/user/delete/" />${user.username}" ref="${user.username}"  class="btn btn-danger"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span></a></td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>

            </div>
            <!-- Table -->

            </c:when>
                <c:otherwise>
                    <div style="margin:20px;"><b>Ничего не найдено</b></div>
                </c:otherwise>
            </c:choose>

        <!-- End Box -->
<form action="" id="changeForm" method="get">
</form>
</body>