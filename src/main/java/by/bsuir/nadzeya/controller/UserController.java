package by.bsuir.nadzeya.controller;

import by.bsuir.nadzeya.constants.Paths;
import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.UserDTO;
import by.bsuir.nadzeya.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.text.ParseException;
import java.util.*;

@Controller
public class UserController {

   @Autowired
   private IUserService userService;

   @Autowired
   private SessionRegistry sessionRegistry;

   @RequestMapping(value = "/users", method = RequestMethod.GET)
   public String listUsers(HttpSession session,
                           Map<String, Object> map) {

      map.put("action" , "users");

      List<UserDTO> users = userService.listUsers();
      for (UserDTO user : users) {
         user.setRoles(userService.listUserRoles(user.getUsername()));
      }

      map.put("usersList", users);

      return Paths.USERS;
   }

   @RequestMapping(value = "/users/user-enable/{name}", method = RequestMethod.GET)
   public String enableUser(@PathVariable("name") String name, Map<String, Object> map) {
      UserDTO user = userService.getUserByName(name);
      user.setEnabled(true);
      userService.save(user);
      return Paths.REDIRECT_USERS;
   }

   @RequestMapping(value = "/users/user-disable/{name}", method = RequestMethod.GET)
   public String disableUser(@PathVariable("name") String name, Map<String, Object> map) {
      UserDTO user = userService.getUserByName(name);
      user.setEnabled(false);
      userService.save(user);

      deactivateUser(name);

      return Paths.REDIRECT_USERS;
   }

   private void deactivateUser(String name) {
      List<SessionInformation> activeSessions = new ArrayList<>();
      for (Object principal : sessionRegistry.getAllPrincipals()) {
         if (principal instanceof User) {
            if (((User) principal).getUsername().equals(name)) {
               for (SessionInformation session : sessionRegistry.getAllSessions(principal, false)) {
                  activeSessions.add(session);
               }
            }
         }
      }
      for (SessionInformation session : activeSessions) {
         session.expireNow();
      }
   }

   @RequestMapping(value = "/settings", method = RequestMethod.GET)
   public String settings(HttpSession session,
                           Map<String, Object> map) {

      map.put("action" , "settings");

      Authentication authentic = SecurityContextHolder.getContext().getAuthentication();

      if (authentic.getName() != null) {

         UserDTO user = userService.getUserByName(authentic.getName());

         map.put("user", user);

      }

      return Paths.USER_SETTINGS;
   }

   @RequestMapping(value = "/settings", method = RequestMethod.POST)
   public String saveUserSettings(@RequestParam(value = "email", required = false) String email,
                                  @RequestParam(value = "password", required = false) String password,
                                  @RequestParam(value = "password_confirm", required = false) String password_confirm,
                                  Map<String, Object> map) throws ParseException {
      Authentication authentic = SecurityContextHolder.getContext().getAuthentication();

      UserDTO user = null;

      if (authentic.getName() != null) {

         user = userService.getUserByName(authentic.getName());

         if (!password.equals(password_confirm)) {
            map.put("error", true);
         } else {
            user.setEmail(email);
            if (!"".equals(password))
               user.setPassword(password);

            userService.save(user);

            map.put("ok", true);
         }

      }

      map.put("user", user);
      return Paths.USER_SETTINGS;
   }


   @RequestMapping(value = "/adduser", method = RequestMethod.GET)
   public String addUser(Map<String, Object> model) {

      model.put("action" , "users");
      model.put("userDTO", new UserDTO());
      return Paths.ADDUSER;
   }

   @RequestMapping(value = "/user/edit/{id}", method = RequestMethod.GET)
   public String editForm(Map<String, Object> model, @PathVariable("id") String id) {
      UserDTO user = userService.getUserByName(id);
      model.put("action" , "users");
      if (user != null) {
         List<String> roles = userService.listUserRoles(user.getUsername());
         if (roles.contains("ROLE_ADMIN")) {
            model.put("is_admin", true);
         }

         model.put("userDTO", user);
      } else {
         return Paths.ROOT_REDIRECT;
      }
      return Paths.ADDUSER;
   }

   @RequestMapping(value = "/adduser", method = RequestMethod.POST)
   public String processForm(@Valid UserDTO user,
                             BindingResult result,
                             @RequestParam("password") String password,
                             @RequestParam("password_confirm") String password_confirm,
                             @RequestParam(value = "is_admin", required = false) Integer is_admin,
                             Map<String, Object> model) {
      model.put("is_admin", is_admin);
      model.put("action" , "users");

      if (result.hasErrors()) {
         return Paths.ADDUSER;
      }
      if (user.getId() == null && password.equals("") || !password_confirm.equals(password)) {
         model.put("error", 1);
         return Paths.ADDUSER;
      }
      UserDTO checkUser = userService.getUserByName(user.getUsername());
      if (user.getId() == null) {
         user.setAdded(new Date());
         user.setId(null);
         if (checkUser != null) {
            model.put("error", 2);
            return Paths.ADDUSER;
         }
      } else {
         UserDTO prevUser = userService.getUser(user.getId());
         user.setAdded(prevUser.getAdded());
         user.setId(prevUser.getId());

         if (checkUser != null && !prevUser.getUsername().equals(user.getUsername())) {
            model.put("error", 2);
            return Paths.ADDUSER;
         }
         if (password.equals("")) {
            user.setPassword(prevUser.getPassword());
         }
      }
      userService.save(user);
      userService.addRole(user.getUsername(), Roles.ROLE_USER);
      if (is_admin != null && is_admin == 1) {
         userService.addRole(user.getUsername(), Roles.ROLE_ADMIN);
      } else {
         userService.removeRole(user.getUsername(), Roles.ROLE_ADMIN);
      }
      return Paths.REDIRECT_USERS;
   }


   @RequestMapping(value = "/user/delete/{id}", method = RequestMethod.GET)
   public String deleteForm(Map<String, Object> model, @PathVariable("id") String id) {
      if (id != null) {
         UserDTO user = userService.getUserByName(id);
         userService.removeUser(user.getId());
      }
      return Paths.REDIRECT_USERS;
   }

}
