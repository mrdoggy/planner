package by.bsuir.nadzeya.controller;

import by.bsuir.nadzeya.constants.Paths;
import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.*;
import by.bsuir.nadzeya.service.ITaskService;
import by.bsuir.nadzeya.service.ITaskVoteService;
import by.bsuir.nadzeya.service.IUserService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.util.*;

@Controller
public class TaskController {

   @Autowired
   private ITaskService taskService;

   @Autowired
   private IUserService userService;

   @Autowired
   private ITaskVoteService taskVoteService;

   @RequestMapping(value = "/")
   public String listTasks(@RequestParam(value = "sort", required = false) String sort,
                           @RequestParam(value = "dir", required = false) String dir,
                           @RequestParam(value = "q", required = false) String q,
                           HttpSession session,
                           Map<String, Object> map) {

      map.put("action" , "tasks");

      List<TaskDTO> tasksList = null;
      if (q != null && !"".equals(q)) {
         tasksList = taskService.filteredList(q);
         map.put("q", q);
      } else {
         tasksList = taskService.list();
      }

      map.put("tasksList", tasksList);

      return Paths.TASKS;
   }

   @RequestMapping(value = "/bookmarks")
   public String listBookmarksTasks(@RequestParam(value = "sort", required = false) String sort,
                           @RequestParam(value = "dir", required = false) String dir,
                           HttpSession session,
                           Map<String, Object> map) {

      map.put("action" , "bookmarks");

      Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
      Long userId = 0L;
      if (authentic.getName() != null) {
         UserDTO user = userService.getUserByName(authentic.getName());
         userId = user.getId();
      }

      List<TaskDTO> tasksList = taskService.getBookmarked(userId);

      map.put("tasksList", tasksList);

      return Paths.BOOKMARKS;
   }

   @RequestMapping("/index")
   public String home() {
      return Paths.ROOT_REDIRECT;
   }

   @RequestMapping(value = "/addtask", method = RequestMethod.GET)
   public String addTask(Map<String, Object> model) {
      Map<String,String> priorityList = generateVotes();

      model.put("action" , "tasks");
      model.put("taskDTO", new TaskDTO());
      model.put("priorityList", priorityList);
      return Paths.ADDTASK;
   }

   @RequestMapping(value = "/task/edit/{id}", method = RequestMethod.GET)
   public String editTask(Map<String, Object> model, @PathVariable("id") String id) {
      model.put("action" , "tasks");
      model.put("priorityList", generateVotes());

      if (id != null && !"".equals(id)) {

         TaskDTO task = taskService.getTask(Long.parseLong(id));
         if (task.getTaskVotes() != null && task.getTaskVotes().size() > 0) {
            List<String> votes = new ArrayList<>();
            for(TaskVoteDTO taskVote : task.getTaskVotes()) {
               votes.add(taskVote.getTitle());
            }
            model.put("votesList", "'" + StringUtils.join(votes, "','") + "'");
         }
         model.put("taskDTO", task);
      } else {
         return Paths.ROOT_REDIRECT;
      }
      return Paths.ADDTASK;
   }

   @RequestMapping(value = "/task/{id}", method = RequestMethod.GET)
   public String showTask(Map<String, Object> model, @PathVariable("id") String id) {
      model.put("action" , "tasks");

      if (id != null && !"".equals(id)) {
         // get user id
         Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
         Long userId = 0L;
         if (authentic.getName() != null) {
            UserDTO user = userService.getUserByName(authentic.getName());
            userId = user.getId();
         }

         //get task info
         TaskDTO task = taskService.getTask(Long.parseLong(id));
         model.put("taskDTO", task);

         // get bookmarks
         List<BookmarkDTO> bookmarks = userService.getBookmarksForUser(userId);
         boolean isBookmarked = false;
         for (BookmarkDTO bookmark: bookmarks) {
            if (bookmark.getTaskId().equals(task.getId()))
               isBookmarked = true;
         }
         model.put("bookmarked", isBookmarked);

         // get comments
         List<CommentDTO> comments = taskService.getCommentsForTask(task.getId());
         model.put("comments", comments);
      } else {
         return Paths.ROOT_REDIRECT;
      }
      return Paths.SHOWTASK;
   }

   private Map<String,String> generateVotes() {
      Map<String,String> priorityList = new LinkedHashMap<String,String>();
      priorityList.put("0","0");
      priorityList.put("1","1");
      priorityList.put("2","2");
      priorityList.put("3","3");
      priorityList.put("4","4");
      priorityList.put("5","5");
      return priorityList;
   }

   @RequestMapping(value = "/addtask", method = RequestMethod.POST)
   public String processForm(@Valid TaskDTO task,
                             BindingResult result,
                             Map<String, Object> model) {
      model.put("action" , "tasks");

      Map<String,String> priorityList = generateVotes();

      model.put("priorityList", priorityList);

      if (result.hasErrors()) {
         model.put("votesList", "'" + StringUtils.join(task.getVotes(), "','") + "'");
         return Paths.ADDTASK;
      }

      Map<String, Integer> taskVotes = new HashMap<>();

      if (task.getId() == null) {
         Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
         if (authentic.getName() != null) {
            UserDTO user = userService.getUserByName(authentic.getName());
            task.setUserId(user.getId());
         }
         task.setAdded(new Date());
         task.setId(null);
      } else {
         TaskDTO currentTask = taskService.getTask(task.getId());
         for(TaskVoteDTO taskVote : currentTask.getTaskVotes()) {
            taskVotes.put(taskVote.getTitle(), taskVote.getVotes());
            taskVoteService.removeTaskVote(taskVote);
         }
      }
      taskService.save(task);

      if (task.getVotes() != null && task.getVotes().size() > 0) {
         for (String vote : task.getVotes()) {
            TaskVoteDTO taskVote = new TaskVoteDTO();
            taskVote.setTaskId(task.getId());
            taskVote.setTitle(vote);
            if (taskVotes.containsKey(vote)) {
               taskVote.setVotes(taskVotes.get(vote));
            } else {
               taskVote.setVotes(0);
            }
            taskVoteService.save(taskVote);
         }
      }

      return Paths.ROOT_REDIRECT;
   }

   @RequestMapping(value = "/task/addvote", method = RequestMethod.POST)
   public String addVote(Map<String, Object> model, @RequestParam(value = "task_id") String task_id, @RequestParam(value = "vote", required = false) String vote) {
      if (!"".equals(task_id) && !"".equals(vote) && vote != null && task_id != null) {
         TaskVoteDTO taskVote = taskVoteService.getTaskVote(Long.parseLong(vote));
         taskVote.setVotes(taskVote.getVotes() + 1);
         taskVoteService.save(taskVote);
         return Paths.SHOWTASK_REDIRECT + task_id + "?vote=ok" ;
      }
      return Paths.SHOWTASK_REDIRECT + task_id  ;
   }

   @RequestMapping(value = "/task/bookmark/add/{id}", method = RequestMethod.GET)
   public String addToBookmark(Map<String, Object> model,  @PathVariable("id") String id) {
      if (!"".equals(id) && id != null) {
         Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
         Long userId = 0L;
         if (authentic.getName() != null) {
            UserDTO user = userService.getUserByName(authentic.getName());
            userId = user.getId();
         }
         userService.addToBookmark(Long.parseLong(id), userId);
      }
      return Paths.SHOWTASK_REDIRECT + id  ;
   }

   @RequestMapping(value = "/task/bookmark/drop/{id}", method = RequestMethod.GET)
   public String dropFromBookmark(Map<String, Object> model,  @PathVariable("id") String id) {
      if (!"".equals(id) && id != null) {
         Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
         Long userId = 0L;
         if (authentic.getName() != null) {
            UserDTO user = userService.getUserByName(authentic.getName());
            userId = user.getId();
         }
         userService.dropFromBookmark(Long.parseLong(id), userId);
      }
      return Paths.SHOWTASK_REDIRECT + id  ;
   }

   @RequestMapping(value = "/task/comment", method = RequestMethod.POST)
   public String addComment(@RequestParam(value = "task_id") String task_id,
                            @RequestParam(value = "comments") String comments,
                            Map<String, Object> model) {
      Authentication authentic = SecurityContextHolder.getContext().getAuthentication();
      Long userId = 0L;
      UserDTO user = null;
      if (authentic.getName() != null) {
         user = userService.getUserByName(authentic.getName());
         userId = user.getId();
      }

      CommentDTO comment = new CommentDTO();
      comment.setUser(user);
      comment.setTaskId(Long.parseLong(task_id));
      comment.setComment(comments);
      comment.setAdded(new Date());
      userService.addComment(comment);

      return Paths.SHOWTASK_REDIRECT + task_id + "?comment=ok" ;
   }

   @RequestMapping(value = "/task/delete/{id}", method = RequestMethod.GET)
   public String deleteTask(Map<String, Object> model, @PathVariable("id") String id) {
      if (id != null) {
         taskService.removeTask(Long.parseLong(id));
      }
      return Paths.ROOT_REDIRECT ;
   }

}
