package by.bsuir.nadzeya.constants;

public final class Paths {
   public static final String ROOT_REDIRECT = "redirect:/";
   public static final String LOGIN_REDIRECT = "redirect:/login.jsp";
   public static final String FORMS = "tasks";
   public static final String ADD = "add";
   public static final String ERRORS = "errors";
   public static final String REDIRECT_USERS = "redirect:/users";
   public static final String ADDUSER = "adduser";
   public static final String USER_SETTINGS = "user_settings";
   public static final String USERS = "users";
   public static final String TASKS = "tasks";
   public static final String BOOKMARKS = "bookmarks";
   public static final String ADDTASK = "addtask";
   public static final String SHOWTASK = "showtask";
   public static final String SHOWTASK_REDIRECT = "redirect:/task/";
}
