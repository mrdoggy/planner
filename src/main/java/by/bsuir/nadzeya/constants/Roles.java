package by.bsuir.nadzeya.constants;

public enum Roles {
   ROLE_USER(2),
   ROLE_ADMIN(3),
   ROLE_ANONYMOUS(1);

   private final int value;

   Roles(int value) {
      this.value = value;
   }

   public int getValue() {
      return value;
   }
}
