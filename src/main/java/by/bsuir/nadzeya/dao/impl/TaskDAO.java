package by.bsuir.nadzeya.dao.impl;

import by.bsuir.nadzeya.dao.ITaskDAO;
import by.bsuir.nadzeya.dto.BookmarkDTO;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.TaskDTO;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TaskDAO  extends HibernateDao<TaskDTO, Long> implements ITaskDAO {

   @Override
   public List<TaskDTO> listFilteredByTaskId(List<Long> tasks) {
      if (tasks != null && tasks.size() > 0)
         return currentSession().createCriteria(TaskDTO.class).add(Restrictions.in("id", tasks)).list();
      else
         return null;
   }

   @Override
   public List<CommentDTO> getCommentsForTask(Long id) {
      return currentSession().createCriteria(CommentDTO.class).add(Restrictions.eq("taskId", id)).list();
   }

   @Override
   public List<TaskDTO> listFilteredByQuery(String q) {
      if (q != null && !"".equals(q)) {
         q = "%" + q + "%";
         return currentSession().createCriteria(TaskDTO.class).add(Restrictions.or(Restrictions.like("title", q), Restrictions.like("description", q))).list();
      }
      else
         return currentSession().createCriteria(TaskDTO.class).list();
   }
}
