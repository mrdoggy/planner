package by.bsuir.nadzeya.dao.impl;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dao.IUserDAO;
import by.bsuir.nadzeya.dto.UserDTO;
import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDAO extends HibernateDao<UserDTO, Long> implements IUserDAO {

   @Autowired
   private SessionFactory sessionFactory;

   @Transactional
   @SuppressWarnings("unchecked")
   public List<String> listUserRoles(String username) {
      Query qry = sessionFactory.getCurrentSession().createSQLQuery(
            "SELECT r.role_name\n"
                  + "                FROM users u, roles r, users_roles a\n"
                  + "                WHERE u.username = a.username\n"
                  + "                AND r.id = a.role_id\n"
                  + "                AND u.username = ?");
      qry.setParameter(0, username);
      return qry.list();
   }

   @Transactional
   public List<String> listRoles() {
      Query qry = sessionFactory.getCurrentSession().createSQLQuery(
            "SELECT id,role_name\n" + "                FROM roles\n");
      return qry.list();
   }

   @Transactional
   @Override
   public void removeUser(Long id) {
      UserDTO user = (UserDTO) sessionFactory.getCurrentSession().get(UserDTO.class, id);
      if (null != user) {
         this.removeRole(user.getUsername(), Roles.ROLE_ADMIN);
         this.removeRole(user.getUsername(), Roles.ROLE_USER);
         sessionFactory.getCurrentSession().delete(user);
      }
   }

   @Transactional
   @Override
   @SuppressWarnings("unchecked")
   public UserDTO getUserByName(String name) {

      Query userQ = sessionFactory.getCurrentSession().createQuery(
            "from UserDTO where username = ?");
      userQ.setParameter(0, name);
      List<UserDTO> result = userQ.list();
      if (result.size() > 0)
         return result.get(0);
      else
         return null;
   }

   @Transactional
   @Override
   public void addRole(String username, Roles role) {

      List<String> roles = this.listUserRoles(username);
      List<Roles> rolesEnum = new ArrayList<>();
      for (String roleStr : roles) {
         rolesEnum.add(Roles.valueOf(roleStr));
      }
      if (!rolesEnum.contains(role)) {
         Query query = sessionFactory.getCurrentSession().createSQLQuery(
               "INSERT INTO users_roles(username,role_id) VALUES(?,?)");
         query.setParameter(0, username);
         query.setParameter(1, role.getValue());
         query.executeUpdate();
      }
   }

   @Transactional
   @Override
   public void removeRole(String username, Roles role) {
      Query query = sessionFactory.getCurrentSession().createSQLQuery(
            "DELETE FROM users_roles WHERE username=? AND role_id=?");
      query.setParameter(0, username);
      query.setParameter(1, role.getValue());
      query.executeUpdate();
   }

   @Override
   @Transactional(propagation = Propagation.REQUIRES_NEW)
   public void changeStatusUsersByRole(Roles roleUser, int status) {
      Query query = sessionFactory.getCurrentSession().createSQLQuery(
            "update users_roles r,\n" +
                  "  users u SET u.enabled=?\n" +
                  "where u.username = r.username\n" +
                  "      and r.role_id = ?   and not exists (select null\n" +
                  "                                          from users_roles r1\n" +
                  "                                          where r1.username = r.username\n" +
                  "                                                and r1.role_id != ?)");
      query.setParameter(0, status);
      query.setParameter(1, roleUser.getValue());
      query.setParameter(2, roleUser.getValue());
      query.executeUpdate();
      sessionFactory.getCurrentSession().flush();
   }

}
