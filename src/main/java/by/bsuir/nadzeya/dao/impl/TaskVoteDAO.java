package by.bsuir.nadzeya.dao.impl;

import by.bsuir.nadzeya.dao.ITaskVoteDAO;
import by.bsuir.nadzeya.dto.TaskVoteDTO;
import org.springframework.stereotype.Repository;

@Repository
public class TaskVoteDAO extends HibernateDao<TaskVoteDTO, Long> implements ITaskVoteDAO {

}
