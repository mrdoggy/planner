package by.bsuir.nadzeya.dao.impl;

import by.bsuir.nadzeya.dao.ICommentDAO;
import by.bsuir.nadzeya.dto.CommentDTO;
import org.springframework.stereotype.Repository;

@Repository
public class CommentDAO extends HibernateDao<CommentDTO, Long> implements ICommentDAO {

}
