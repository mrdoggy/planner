package by.bsuir.nadzeya.dao.impl;

import by.bsuir.nadzeya.dao.IBookmarkDAO;
import by.bsuir.nadzeya.dao.ITaskVoteDAO;
import by.bsuir.nadzeya.dto.BookmarkDTO;
import by.bsuir.nadzeya.dto.TaskVoteDTO;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BookmarkDAO extends HibernateDao<BookmarkDTO, Long> implements IBookmarkDAO {

   @Override
   public void removeBookmarks(Long taskId, Long userId) {
      String hql = "delete from BookmarkDTO f where f.taskId = :taskId and f.userId = :userId";
      currentSession().createQuery(hql).setParameter("taskId", taskId).setParameter("userId", userId).executeUpdate();
   }

   @Override
   public List<BookmarkDTO> getBookmarksForUser(Long userId) {
      return currentSession().createCriteria(BookmarkDTO.class).add(Restrictions.eq("userId", userId)).list();
   }
}
