package by.bsuir.nadzeya.dao;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.UserDTO;

import java.util.List;
import java.util.Map;

public interface IUserDAO extends GenericDao<UserDTO, Long> {

   List<String> listUserRoles(String username);

   List<String> listRoles();

   void removeUser(Long id);

   UserDTO getUserByName(String name);

   void addRole(String username, Roles role);

   void removeRole(String username, Roles role);

   void changeStatusUsersByRole(Roles roleUser, int status);
}
