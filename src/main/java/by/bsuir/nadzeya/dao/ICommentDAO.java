package by.bsuir.nadzeya.dao;

import by.bsuir.nadzeya.dto.CommentDTO;

public interface ICommentDAO extends GenericDao<CommentDTO, Long>  {


}
