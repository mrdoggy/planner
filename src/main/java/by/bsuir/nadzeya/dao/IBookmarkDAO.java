package by.bsuir.nadzeya.dao;

import by.bsuir.nadzeya.dto.BookmarkDTO;

import java.util.List;

public interface IBookmarkDAO extends GenericDao<BookmarkDTO, Long>  {

   void removeBookmarks(Long taskId, Long userId);

   List<BookmarkDTO> getBookmarksForUser(Long userId);
}
