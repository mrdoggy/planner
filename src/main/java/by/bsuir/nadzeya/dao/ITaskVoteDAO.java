package by.bsuir.nadzeya.dao;

import by.bsuir.nadzeya.dto.TaskDTO;
import by.bsuir.nadzeya.dto.TaskVoteDTO;

public interface ITaskVoteDAO extends GenericDao<TaskVoteDTO, Long>  {


}
