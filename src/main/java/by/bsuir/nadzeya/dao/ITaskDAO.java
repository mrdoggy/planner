package by.bsuir.nadzeya.dao;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.TaskDTO;

import java.util.List;

public interface ITaskDAO extends GenericDao<TaskDTO, Long>  {

   List<TaskDTO> listFilteredByTaskId(List<Long> tasks);

   List<CommentDTO> getCommentsForTask(Long id);

   List<TaskDTO> listFilteredByQuery(String q);
}
