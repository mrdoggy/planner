package by.bsuir.nadzeya.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import java.util.Date;

/**
 * @author alex on 07.12.15.
 */
@Entity
@Table(name = "comments")
public class CommentDTO {
   @Id
   @Column(name = "ID", unique = true, nullable = false)
   @GeneratedValue
   private Long id;

   @Column(name = "TASK_ID")
   private Long taskId;

   @ManyToOne(fetch = FetchType.EAGER)
   @Fetch(FetchMode.SELECT)
   private UserDTO user;

   @Column(name = "ADDED")
   private Date added;

   @Column(name = "COMMENT")
   private String comment;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Long getTaskId() {
      return taskId;
   }

   public void setTaskId(Long taskId) {
      this.taskId = taskId;
   }

   public UserDTO getUser() {
      return user;
   }

   public void setUser(UserDTO user) {
      this.user = user;
   }

   public Date getAdded() {
      return added;
   }

   public void setAdded(Date added) {
      this.added = added;
   }

   public String getComment() {
      return comment;
   }

   public void setComment(String comment) {
      this.comment = comment;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      CommentDTO that = (CommentDTO) o;

      if (comment != null ? !comment.equals(that.comment) : that.comment != null) return false;
      if (added != null ? !added.equals(that.added) : that.added != null) return false;
      if (id != null ? !id.equals(that.id) : that.id != null) return false;
      if (taskId != null ? !taskId.equals(that.taskId) : that.taskId != null) return false;
      if (user != null ? !user.equals(that.user) : that.user != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (taskId != null ? taskId.hashCode() : 0);
      result = 31 * result + (user != null ? user.hashCode() : 0);
      result = 31 * result + (added != null ? added.hashCode() : 0);
      result = 31 * result + (comment != null ? comment.hashCode() : 0);
      return result;
   }
}
