package by.bsuir.nadzeya.dto;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.SortNatural;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.Future;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.*;

@Entity
@Table(name = "tasks")
public class TaskDTO {

   @Id
   @Column(name = "ID", unique = true, nullable = false)
   @GeneratedValue
   private Long id;

   @Size(min = 1, max = 200, message = "{errors.requiredfield}")
   @Column(name = "TITLE")
   private String title;

   @Column(name = "ADDED")
   private Date added;

   @Column(name = "USER_ID")
   private Long userId;

   @Column(name = "PRIORITY")
   private Integer priority;

   @Column(name = "DESCRIPTION")
   private String description;

   @Column(name = "DATE_END")
   @Future(message = "{errors.incorrectDateEnd}")
   @DateTimeFormat(pattern="yyyy-MM-dd")
   private Date dateEnd;

   @OneToMany(fetch = FetchType.EAGER, mappedBy = "taskId")
   @Fetch(FetchMode.SELECT)
   @SortNatural
   private Set<TaskVoteDTO> taskVotes = new TreeSet<>();

   @Transient
   @NotNull(message = "{errors.requiredfield}")
   private List<String> votes;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public Date getAdded() {
      return added;
   }

   public void setAdded(Date added) {
      this.added = added;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }

   public Integer getPriority() {
      return priority;
   }

   public void setPriority(Integer priority) {
      this.priority = priority;
   }

   public Date getDateEnd() {
      return dateEnd;
   }

   public void setDateEnd(Date dateEnd) {
      this.dateEnd = dateEnd;
   }

   public Set<TaskVoteDTO> getTaskVotes() {
      return taskVotes;
   }

   public void setTaskVotes(Set<TaskVoteDTO> taskVotes) {
      this.taskVotes = taskVotes;
   }

   public String getDescription() {
      return description;
   }

   public void setDescription(String description) {
      this.description = description;
   }

   public List<String> getVotes() {
      return votes;
   }

   public void setVotes(List<String> votes) {
      this.votes = votes;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      TaskDTO taskDTO = (TaskDTO) o;

      if (!added.equals(taskDTO.added)) return false;
      if (dateEnd != null ? !dateEnd.equals(taskDTO.dateEnd) : taskDTO.dateEnd != null) return false;
      if (description != null ? !description.equals(taskDTO.description) : taskDTO.description != null) return false;
      if (!id.equals(taskDTO.id)) return false;
      if (priority != null ? !priority.equals(taskDTO.priority) : taskDTO.priority != null) return false;
      if (taskVotes != null ? !taskVotes.equals(taskDTO.taskVotes) : taskDTO.taskVotes != null) return false;
      if (!title.equals(taskDTO.title)) return false;
      if (userId != null ? !userId.equals(taskDTO.userId) : taskDTO.userId != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      int result = (id != null ? id.hashCode() : 0);
      result = 31 * result + (title != null ? title.hashCode() : 0);
      result = 31 * result + (added != null ? added.hashCode() : 0);
      result = 31 * result + (userId != null ? userId.hashCode() : 0);
      result = 31 * result + (priority != null ? priority.hashCode() : 0);
      result = 31 * result + (description != null ? description.hashCode() : 0);
      result = 31 * result + (dateEnd != null ? dateEnd.hashCode() : 0);
      result = 31 * result + (taskVotes != null ? taskVotes.hashCode() : 0);
      return result;
   }
}
