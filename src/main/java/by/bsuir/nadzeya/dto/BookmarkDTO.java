package by.bsuir.nadzeya.dto;

import javax.persistence.*;

@Entity
@Table(name = "bookmarks")
public class BookmarkDTO {
   @Id
   @Column(name = "ID", unique = true, nullable = false)
   @GeneratedValue
   private Long id;

   @Column(name = "TASK_ID")
   private Long taskId;

   @Column(name = "USER_ID")
   private Long userId;

   public Long getId() {

      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public Long getTaskId() {
      return taskId;
   }

   public void setTaskId(Long taskId) {
      this.taskId = taskId;
   }

   public Long getUserId() {
      return userId;
   }

   public void setUserId(Long userId) {
      this.userId = userId;
   }


   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      BookmarkDTO that = (BookmarkDTO) o;

      if (id != null ? !id.equals(that.id) : that.id != null) return false;
      if (taskId != null ? !taskId.equals(that.taskId) : that.taskId != null) return false;
      if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      int result = id != null ? id.hashCode() : 0;
      result = 31 * result + (taskId != null ? taskId.hashCode() : 0);
      result = 31 * result + (userId != null ? userId.hashCode() : 0);
      return result;
   }

}
