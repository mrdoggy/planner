package by.bsuir.nadzeya.dto;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Created on 12/4/2015.
 */
@Entity
@Table(name = "tasks_votes")
public class TaskVoteDTO implements Comparable {
   @Id
   @Column(name = "ID", unique = true, nullable = false)
   @GeneratedValue
   private Long id;

   @Size(min = 1, max = 200, message = "{errors.requiredfield}")
   @Column(name = "TITLE")
   private String title;

   @Column(name = "TASK_ID")
   private Long taskId;

   @Column(name = "VOTES")
   private Integer votes;

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getTitle() {
      return title;
   }

   public void setTitle(String title) {
      this.title = title;
   }

   public Long getTaskId() {
      return taskId;
   }

   public void setTaskId(Long taskId) {
      this.taskId = taskId;
   }

   public Integer getVotes() {
      return votes;
   }

   public void setVotes(Integer votes) {
      this.votes = votes;
   }


   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      TaskVoteDTO that = (TaskVoteDTO) o;

      if (!id.equals(that.id)) return false;
      if (taskId != null ? !taskId.equals(that.taskId) : that.taskId != null) return false;
      if (!title.equals(that.title)) return false;
      if (votes != null ? !votes.equals(that.votes) : that.votes != null) return false;

      return true;
   }

   @Override
   public int hashCode() {
      int result = (id != null ? id.hashCode() : 31);
      result = 31 * result + (title != null ? title.hashCode() : 0);
      result = 31 * result + (taskId != null ? taskId.hashCode() : 0);
      result = 31 * result + (votes != null ? votes.hashCode() : 0);
      return result;
   }

   @Override
   public int compareTo(Object o) {
      if (o instanceof TaskVoteDTO && this.title != null) {
         return this.title.compareTo(((TaskVoteDTO) o).getTitle());
      }
      return 0;
   }
}
