package by.bsuir.nadzeya.dto;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "users")
public class UserDTO {
   @Id
   @Column(name = "ID")
   @GeneratedValue
   private Long id;

   @Size(min = 1, max = 100, message = "{errors.requiredfield}")
   @Column(name = "USERNAME")
   private String username;

   @Column(name = "PASSWORD")
   private String password;

   @Column(name = "ADDED")
   private Date added;

   @Pattern(regexp = "^(?:[a-zA-Z0-9_'^&/+-])+(?:\\.(?:[a-zA-Z0-9_'^&/+-])+)" +
         "*@(?:(?:\\[?(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))\\.)" +
         "{3}(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\]?)|(?:[a-zA-Z0-9-]+\\.)" +
         "+(?:[a-zA-Z]){2,}\\.?)$",
         message = "{errors.email}")
   @Column(name = "EMAIL")
   private String email;

   @Column(name = "TYPE")
   private Integer type;

   @Column(name = "ENABLED")
   private boolean enabled;

   @Column(name = "SETTINGS")
   private String settings;

   @Transient
   private List<String> roles = new ArrayList<>();

   public List<String> getRoles() {
      return roles;
   }

   public void setRoles(List<String> roles) {
      this.roles = roles;
   }

   public boolean getEnabled() {
      return enabled;
   }

   public void setEnabled(boolean enabled) {
      this.enabled = enabled;
   }

   public Long getId() {
      return id;
   }

   public void setId(Long id) {
      this.id = id;
   }

   public String getUsername() {
      return username;
   }

   public void setUsername(String username) {
      this.username = username;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public Date getAdded() {
      return (Date) added.clone();
   }

   public void setAdded(Date added) {
      this.added = (Date) added.clone();
   }

   public String getEmail() {
      return email;
   }

   public void setEmail(String email) {
      this.email = email;
   }

   public Integer getType() {
      return type;
   }

   public void setType(Integer type) {
      this.type = type;
   }

   public String getSettings() {
      return settings;
   }

   public void setSettings(String settings) {
      this.settings = settings;
   }


   @Override
   public int hashCode() {
      int result = (id != null ? id.hashCode() : 31);
      result = 31 * result + (enabled ? 1 : 0);
      result = 31 * result + (email != null ? email.hashCode() : 0);
      result = 31 * result + (id != null ? id.intValue() : 0);
      result = 31 * result + (password != null ? password.hashCode() : 0);
      result = 31 * result + (roles != null ? roles.hashCode() : 0);
      result = 31 * result + (settings != null ? settings.hashCode() : 0);
      result = 31 * result + (type != null ? type : 0);
      result = 31 * result + (username != null ? username.hashCode() : 0);
      return result;
   }

   @Override
   public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      UserDTO user = (UserDTO) o;

      if (enabled != user.enabled) return false;
      if (email != null ? !email.equals(user.email) : user.email != null) return false;
      if (!id.equals(user.id)) return false;
      if (password != null ? !password.equals(user.password) : user.password != null) return false;
      if (roles != null ? !roles.equals(user.roles) : user.roles != null) return false;
      if (settings != null ? !settings.equals(user.settings) : user.settings != null) return false;
      if (type != null ? !type.equals(user.type) : user.type != null) return false;
      return username.equals(user.username);

   }


}
