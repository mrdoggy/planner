package by.bsuir.nadzeya.service.impl;

import by.bsuir.nadzeya.dao.IBookmarkDAO;
import by.bsuir.nadzeya.dao.ITaskDAO;
import by.bsuir.nadzeya.dto.BookmarkDTO;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.TaskDTO;
import by.bsuir.nadzeya.service.ITaskService;
import by.bsuir.nadzeya.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TaskService implements ITaskService {

   @Autowired
   private ITaskDAO taskDAO;

   @Autowired
   private IUserService userService;

   @Override
   public void save(TaskDTO task) {
      taskDAO.update(task);
   }

   @Override
   public List<TaskDTO> list() {
      return taskDAO.list();
   }

   @Override
   public void removeTask(Long id) {
      TaskDTO task = this.getTask(id);
      if (task != null) {
         taskDAO.remove(task);
      }
   }

   @Override
   public TaskDTO getTask(Long id) {
      return taskDAO.find(id);
   }

   @Override
   public List<TaskDTO> getBookmarked(Long userId) {
      List<BookmarkDTO> bookmarks = userService.getBookmarksForUser(userId);
      List<Long> tasks = new ArrayList<>();
      for(BookmarkDTO bookmarkDTO : bookmarks) {
         tasks.add(bookmarkDTO.getTaskId());
      }
      return taskDAO.listFilteredByTaskId(tasks);
   }

   @Override
   public List<CommentDTO> getCommentsForTask(Long id) {
      List<CommentDTO> comments = taskDAO.getCommentsForTask(id);

      return comments;
   }

   @Override
   public List<TaskDTO> filteredList(String q) {
      return taskDAO.listFilteredByQuery(q);
   }
}
