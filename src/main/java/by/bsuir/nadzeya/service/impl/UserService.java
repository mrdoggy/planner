package by.bsuir.nadzeya.service.impl;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dao.IBookmarkDAO;
import by.bsuir.nadzeya.dao.ICommentDAO;
import by.bsuir.nadzeya.dao.IUserDAO;
import by.bsuir.nadzeya.dao.impl.BookmarkDAO;
import by.bsuir.nadzeya.dto.BookmarkDTO;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.UserDTO;
import by.bsuir.nadzeya.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {
   @Autowired
   private IUserDAO userDAO;

   @Autowired
   private IBookmarkDAO bookmarkDAO;

   @Autowired
   private ICommentDAO commentDAO;

   public void save(UserDTO user) {
      userDAO.update(user);
   }

   public List<UserDTO> listUsers() {
      return userDAO.list();
   }

   public List<String> listUserRoles(String username) {
      return userDAO.listUserRoles(username);
   }

   public void removeUser(Long id) {
      userDAO.removeUser(id);
   }

   public UserDTO getUserByName(String name) {
      return userDAO.getUserByName(name);
   }

   public UserDTO getUser(Long id) {
      return userDAO.find(id);
   }

   public void addRole(String username, Roles role) {
      userDAO.addRole(username, role);
   }

   public void removeRole(String username, Roles role) {
      userDAO.removeRole(username, role);
   }

   @Override
   public void changeUsersStatus(Roles roleUser, int status) {
      userDAO.changeStatusUsersByRole(roleUser, status);
   }

   @Override
   public void addToBookmark(Long taskId, Long userId) {
      BookmarkDTO bookmarkDTO = new BookmarkDTO();
      bookmarkDTO.setTaskId(taskId);
      bookmarkDTO.setUserId(userId);
      bookmarkDAO.add(bookmarkDTO);
   }

   @Override
   public void dropFromBookmark(Long taskId, Long userId) {
      bookmarkDAO.removeBookmarks(taskId, userId);
   }

   @Override
   public List<BookmarkDTO> getBookmarksForUser(Long userId) {
      return bookmarkDAO.getBookmarksForUser(userId);
   }

   @Override
   public void addComment(CommentDTO comment) {
      commentDAO.add(comment);
   }
}
