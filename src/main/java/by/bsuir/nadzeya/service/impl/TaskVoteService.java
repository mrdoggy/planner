package by.bsuir.nadzeya.service.impl;

import by.bsuir.nadzeya.dao.ITaskDAO;
import by.bsuir.nadzeya.dao.ITaskVoteDAO;
import by.bsuir.nadzeya.dto.TaskDTO;
import by.bsuir.nadzeya.dto.TaskVoteDTO;
import by.bsuir.nadzeya.service.ITaskService;
import by.bsuir.nadzeya.service.ITaskVoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TaskVoteService implements ITaskVoteService {

   @Autowired
   private ITaskVoteDAO taskVoteDAO;

   @Override
   public void save(TaskVoteDTO task) {
      taskVoteDAO.update(task);
   }

   @Override
   public List<TaskVoteDTO> list() {
      return taskVoteDAO.list();
   }

   @Override
   public void removeTaskVote(TaskVoteDTO taskVote) {
      if (taskVote != null) {
         taskVoteDAO.remove(taskVote);
      }
   }

   @Override
   public TaskVoteDTO getTaskVote(Long id) {
      return taskVoteDAO.find(id);
   }
}
