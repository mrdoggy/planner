package by.bsuir.nadzeya.service;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.BookmarkDTO;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.UserDTO;

import java.util.List;
import java.util.Map;

public interface IUserService {

   void save(UserDTO user);

   List<UserDTO> listUsers();

   List<String> listUserRoles(String username);

   void removeUser(Long id);

   UserDTO getUserByName(String name);

   UserDTO getUser(Long id);

   void addRole(String username, Roles role);

   void removeRole(String username, Roles role);

   void changeUsersStatus(Roles roleUser, int status);

   void addToBookmark(Long taskId, Long userId);

   void dropFromBookmark(Long taskId, Long userId);

   List<BookmarkDTO> getBookmarksForUser(Long userId);

   void addComment(CommentDTO comment);
}
