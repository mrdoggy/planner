package by.bsuir.nadzeya.service;

import by.bsuir.nadzeya.dto.TaskDTO;
import by.bsuir.nadzeya.dto.TaskVoteDTO;

import java.util.List;

public interface ITaskVoteService {
   void save(TaskVoteDTO taskVote);

   List<TaskVoteDTO> list();

   void removeTaskVote(TaskVoteDTO taskVote);

   TaskVoteDTO getTaskVote(Long id);

}
