package by.bsuir.nadzeya.service;

import by.bsuir.nadzeya.constants.Roles;
import by.bsuir.nadzeya.dto.CommentDTO;
import by.bsuir.nadzeya.dto.TaskDTO;

import java.util.List;

public interface ITaskService {
   void save(TaskDTO user);

   List<TaskDTO> list();

   void removeTask(Long id);

   TaskDTO getTask(Long id);

   List<TaskDTO> getBookmarked(Long userId);

   List<CommentDTO> getCommentsForTask(Long id);

   List<TaskDTO> filteredList(String q);
}
